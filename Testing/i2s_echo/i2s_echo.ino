//Based on https://dronebotworkshop.com/esp32-i2s/
//and https://github.com/espressif/esp-idf/blob/af805df3cb6117cc74f0875831fbab6446824453/examples/peripherals/i2s/i2s_basic/i2s_std/main/i2s_std_example_main.c

//ESP32 I2S docs:
//https://docs.espressif.com/projects/esp-idf/en/v4.1/api-reference/peripherals/i2s.html#_CPPv49i2s_write10i2s_port_tPKv6size_tP6size_t10TickType_t

//This code should read the ADC and write the sample to I2S at a rate of 40 kHz

#include <driver/i2s.h>

#define I2S_WS 25
#define I2S_SD 33
#define I2S_SCK 32

#define INPUT_PIN 16 //CHANGE THIS!!!!!!!!!!!!!

// Use I2S Processor 0
#define I2S_PORT I2S_NUM_0
 
// Define input buffer length
#define bufferLen 64
int16_t sBuffer[bufferLen];
 
void i2s_install() {
  // Set up I2S Processor configuration
  const i2s_config_t i2s_config = {
    .mode = i2s_mode_t(I2S_MODE_MASTER | I2S_MODE_TX),
    .sample_rate = 40000, //44100
    .bits_per_sample = i2s_bits_per_sample_t(16),
    .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,
    .communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_I2S),
    .intr_alloc_flags = 0,
    .dma_buf_count = 8,
    .dma_buf_len = bufferLen,
    .use_apll = false,
    .tx_desc_auto_clear = true
  };
 
  i2s_driver_install(I2S_PORT, &i2s_config, 0, NULL);
}
 
void i2s_setpin() {
  // Set I2S pin configuration
  const i2s_pin_config_t pin_config = {
    .bck_io_num = I2S_SCK,
    .ws_io_num = I2S_WS,
    .data_out_num = -1,
    .data_in_num = I2S_SD
  };
 
  i2s_set_pin(I2S_PORT, &pin_config);
}

void IRAM_ATTR Timer0_ISR()
{
    //auto init = ESP_ERROR_CHECK(i2s_channel_enable(tx_chan));
    uint16_t sample = analogRead(INPUT_PIN);
    size_t i2s_bytes_write;

    /* Write i2s data */
    if (i2s_write(I2S_PORT, &sample, 2, &i2s_bytes_write, 100) == ESP_OK) {
      
        printf("Write Task: i2s write %d bytes\n", i2s_bytes_write);
    } else {
        printf("Write Task: i2s write failed\n");
    }
}

void setup() {
  //pinMode(LED, OUTPUT);
  //Set up I2S
  i2s_install();
  i2s_setpin();
  i2s_start(I2S_PORT);

  //Set up timer
  hw_timer_t *Timer0_Cfg = timerBegin(0, 40, true); //Base clock: 80 MHz, Prescaler: 40 
  timerAttachInterrupt(Timer0_Cfg, &Timer0_ISR, true);
  timerAlarmWrite(Timer0_Cfg, 5, true); //... -> 5 ticks corresponds to 40 kHz
  timerAlarmEnable(Timer0_Cfg);
}

void loop() {
  // put your main code here, to run repeatedly:

}
