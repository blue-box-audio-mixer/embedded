#define UART1_RX 27
#define UART1_TX 14
#define UART2_RX 19
#define UART2_TX 18

#define VC_CS 32
#define VC_CLK 33
#define VC_SD 25
#define VC_MUTE 26

const char * cmd_AT          = "AT+\0";

class PGA2311{
  private:
  uint8_t cs;
  uint8_t clk;
  uint8_t sd;
  uint8_t mute_pin;
  bool muted;

  public:
  PGA2311(uint8_t cs, uint8_t clk, uint8_t sd, uint8_t mute_pin){
    this->cs = cs;
    this->clk = clk;
    this->sd = sd;
    this->mute_pin = mute_pin;

    this->muted = false;
  }

  void init(){
    pinMode(cs, OUTPUT);
    pinMode(clk, OUTPUT);
    pinMode(sd, OUTPUT);
    pinMode(mute_pin, OUTPUT);

    digitalWrite(cs, HIGH);
    digitalWrite(clk, HIGH);
    digitalWrite(sd, HIGH);
    digitalWrite(mute_pin, HIGH);
  }

  void send_vol(uint8_t l_vol, uint8_t r_vol){
    digitalWrite(cs, LOW);
    uint16_t vol = 0x0000;
    vol |= (r_vol << 8);
    vol |= l_vol;
    
    for(int i = 0; i < 16; i++){
      digitalWrite(clk, HIGH);
      digitalWrite(sd, (vol & 0x8000) >> 15);
      digitalWrite(clk, LOW);
      vol = vol << 1;
    }
    digitalWrite(cs, HIGH);
    digitalWrite(clk, HIGH);
    digitalWrite(sd, HIGH);
    digitalWrite(mute_pin, HIGH);
  }

  void toggle_mute(){
    muted = !muted;
    digitalWrite(mute_pin, muted ? LOW : HIGH);
  }
    
};

PGA2311 vc_chip(VC_CS, VC_CLK, VC_SD, VC_MUTE);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);  
  Serial1.begin(9600, SERIAL_8N1, UART1_RX, UART1_TX);
  Serial2.begin(9600, SERIAL_8N1, UART2_RX, UART2_TX);

  vc_chip.init();

}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial1.available()){
    Serial.print(static_cast<char>(Serial1.read()));
  }

  if(Serial.available()){
    int c = Serial.read();
    /*if(c != 0x0A){
      Serial1.print(c);
      Serial2.print(c);
    }*/
    if(c == 'A'){
      Serial.println("Sending...");
      Serial1.write(cmd_AT);
    }
    else if(c == 'H'){
      vc_chip.send_vol(127, 127);
    }
    else if(c == 'L'){
      vc_chip.send_vol(255, 255);
    }
    else if(c == 'M'){
      //vc_chip.send_vol(0, 0);
      vc_chip.toggle_mute();
    }
    else if(c == 'V'){
      uint8_t vol = Serial.parseInt();
      vc_chip.send_vol(vol, vol);
    }
  }

}
