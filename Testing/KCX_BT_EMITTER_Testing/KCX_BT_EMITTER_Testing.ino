#include "driver/ledc.h"
#include "esp_err.h"
#include "BluetoothSerial.h"

#define LEDC_TIMER              LEDC_TIMER_0
#define LEDC_MODE               LEDC_LOW_SPEED_MODE
#define LEDC_OUTPUT_IO          (32) // Define the output GPIO
#define LEDC_CHANNEL            LEDC_CHANNEL_0
#define LEDC_DUTY_RES           LEDC_TIMER_13_BIT // Set duty resolution to 13 bits
#define LEDC_DUTY               (4095) // Set duty to 50%. ((2 ** 13) - 1) * 50% = 4095
#define LEDC_FREQUENCY          (5000) // Frequency in Hertz. Set frequency at 5 kHz

#define UART1_RX 27
#define UART1_TX 14
#define UART2_RX 16
#define UART2_TX 17

/*
Bluetooth commands
*/
#define CMD_SET_VOL     0x00
#define CMD_MUTE        0x02
#define CMD_UNMUTE      0x04
#define CMD_CONNECT     0x06
#define CMD_DISCONN     0x08
#define CMD_CONN_STATUS 0x0A
#define CMD_SCAN        0x0C
#define CMD_PAIR        0x0E
#define CMD_UNPAIR      0x10
#define CMD_LIST_PAIRED 0x12
#define CMD_CHN_STATUS  0x14

/*
Channels
*/
#define CHANNEL_A  0x00
#define CHANNEL_B 0x01

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

#if !defined(CONFIG_BT_SPP_ENABLED)
#error Serial Bluetooth not available or not enabled. It is only available for the ESP32 chip.
#endif

enum Channel_State_Flags{
  CHANNEL_ID = 1 << 0,
  MUTED      = 1 << 1,  
  CONNECTED  = 1 << 2
};

class Channel{
  BluetoothSerial* BluetoothAdapter;
  
  uint8_t state_bits;
  uint8_t vol;

  /*Temporary*/
  ledc_channel_t chn;
  
  public:
  Channel(uint8_t channel_id, BluetoothSerial* adapter){
    BluetoothAdapter = adapter;
    state_bits = channel_id;
    vol = 0;

    /*Temporary*/
    chn = ((channel_id == 0x00) ?  LEDC_CHANNEL_0 : LEDC_CHANNEL_1);
  }

  /* buf should be of size 2.*/
  void get_state(uint8_t* buf){
    buf[0] = state_bits;
    buf[1] = vol;
  }

  void set_vol(uint8_t new_vol){
    vol = new_vol;
    uint32_t duty = vol * (8191)/255.0;
    ledc_channel_t a;
    ledc_set_duty(LEDC_MODE, chn, duty);
    ledc_update_duty(LEDC_MODE, chn);        
  }

  void mute(){
    state_bits |= MUTED; 
    ledc_set_duty(LEDC_MODE, chn, 0);
    ledc_update_duty(LEDC_MODE, chn);       
  }  

  void unmute(){
    state_bits &= ~(MUTED);
    set_vol(vol);
  }
  
};

BluetoothSerial SerialBT;

Channel channel_a  = Channel(CHANNEL_A, &SerialBT);
Channel channel_b = Channel(CHANNEL_B,&SerialBT);

void setup() {
  Serial.begin(115200);  
  Serial1.begin(9600, SERIAL_8N1, UART1_RX, UART1_TX);
  SerialBT.begin("ESP32test"); //Bluetooth device name

  /*
  Set up the ESP32's LED controller to generate a test audio signal
  */

  // Prepare and then apply the LEDC PWM timer configuration

  ledc_timer_config_t ledc_timer = {
    /*speed_mode:*/      LEDC_MODE, 
    /*duty_resolution:*/ LEDC_DUTY_RES,
    /*timer_num:*/       LEDC_TIMER, 
    /*freq_hz:*/         LEDC_FREQUENCY
  };
  ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer));

  ledc_timer.timer_num = LEDC_TIMER_1;
  ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer));

  // Prepare and then apply the LEDC PWM channel configuration
  ledc_channel_config_t ledc_channel = {
    LEDC_OUTPUT_IO,
    LEDC_MODE,
    LEDC_CHANNEL_0,
    LEDC_INTR_DISABLE,
    LEDC_TIMER,
    0, // Set duty to 0%
    0
  };

  ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel));
  channel_a.set_vol(127);

  ledc_channel.channel = LEDC_CHANNEL_1;
  ledc_channel.gpio_num = 25;
  
  ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel));
  channel_b.set_vol(127);
}

uint8_t BT_read_byte(){
  while(SerialBT.available() == 0){}
  return static_cast<uint8_t>(SerialBT.read());
}

void BT_read_bytes(uint8_t* out_bytes, uint32_t num_bytes){
  uint32_t num_bytes_read = 0;
  while(num_bytes_read < num_bytes){
    out_bytes[num_bytes_read++] = BT_read_byte();
  }
}

void loop() {
  /*
  ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, LEDC_DUTY);
  ledc_update_duty(LEDC_MODE, LEDC_CHANNEL);
  delay(1000);
  ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, LEDC_DUTY/2);
  ledc_update_duty(LEDC_MODE, LEDC_CHANNEL);
  delay(1000);
  */
  uint8_t data = BT_read_byte();
  uint8_t cmd  = data & 0xFE;
  uint8_t chn  = data & 0x01;

  switch(cmd){
    case CMD_SET_VOL:
    {
      uint8_t data = BT_read_byte();
      switch(chn){
        case CHANNEL_A:
          channel_a.set_vol(data);
          break;
        case CHANNEL_B:
          channel_b.set_vol(data);
          break;
      }
    }
      break;

    case CMD_MUTE:
    
      switch(chn){
        case CHANNEL_A:
          channel_a.mute();          
          break;
        case CHANNEL_B:
          channel_b.mute();
          break;
      }

      break;

    case CMD_UNMUTE:

      switch(chn){
        case CHANNEL_A:
          channel_a.unmute();          
          break;
        case CHANNEL_B:
          channel_b.unmute();
          break;
      }

      break;
    case CMD_CONNECT:
    {
      uint8_t t[4] = {'W', 'o', 'w', '!'};
      SerialBT.write(t, 4);
      break;
    }
    case CMD_DISCONN:
      break;
    case CMD_CONN_STATUS:
      break;
    case CMD_SCAN:
      break;
    case CMD_PAIR:
      break;
    case CMD_UNPAIR:
      break;
    case CMD_LIST_PAIRED:
      {
        char buf[65] = "0012345678901devicewithareallylongnameaaaaaaaaaaaaaaaapleasehelp";
        buf[0] = data;
        
        SerialBT.write((uint8_t*)buf, 64);

        delay(40);

        char buf2[26] = "0819146678903other_device";
        buf2[0] = data;
        
        SerialBT.write((uint8_t*)buf2, 25);

        delay(40);

        char buf3[26] = "0819146678903third_device";
        buf3[0] = data;
        
        SerialBT.write((uint8_t*)buf3, 25);
      }

      break;

    case CMD_CHN_STATUS:
      {
        uint8_t status_buf[5] = {data, 0x00, 0x00, 0x00, 0x00};
        channel_a.get_state(status_buf + 1);
        channel_b.get_state(status_buf + 3);
        SerialBT.write(status_buf, 5);          
      }
      break;
    default:
      Serial.println("Unknown command " + String(static_cast<int>(cmd)));
  }  
  
  /*
  uint32_t duty = static_cast<short>(SerialBT.read()) * (8191)/255.0;
  ledc_set_duty(LEDC_MODE, LEDC_CHANNEL, duty);
  ledc_update_duty(LEDC_MODE, LEDC_CHANNEL);
  */

  /*
  while(Serial.available() == 0){}
  
  uint32_t new_freq = static_cast<uint32_t>(Serial.parseInt());
  if(new_freq != 0)
  {
    Serial.println("Updating freq...");
    Serial.println(new_freq);
    ESP_ERROR_CHECK(ledc_set_freq(LEDC_MODE, LEDC_TIMER, new_freq));
  }
  */
}
