The final embedded code is contained in BlueBox/Blue_Box.ino.
The Info directory contains documents that kept track of pin assignments. These are no longer necessary or relevant.
The Testing directory contains various test sketches that were created during the development process. These are also not relevant to the final product.
