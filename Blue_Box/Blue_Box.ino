/*
Blue_Box.ino is the embedded firmware that runs on the Blue Box.
It responds to the commands sent to it by the phone.

It can:
-Adjust the volume of each speaker
-Mute/unmute each speaker
-Tell each transmitter to pair with the next available speaker

Author: Liam Helfrich (whn7g@mst.edu)
*/

#include "BluetoothSerial.h"

/*
Pins
*/
#define CH1_VC_CS 32
#define CH1_VC_CLK 33
#define CH1_VC_SD 25
#define CH1_VC_MUTE 26
#define CH1_TX_CONNECT 27

#define CH2_VC_CS 23
#define CH2_VC_CLK 22
#define CH2_VC_SD 21
#define CH2_VC_MUTE 19
#define CH2_TX_CONNECT 18

/*
Bluetooth commands
*/
#define CMD_SET_VOL     0x00
#define CMD_MUTE        0x02
#define CMD_UNMUTE      0x04
#define CMD_CONNECT     0x06
#define CMD_DISCONN     0x08
#define CMD_CONN_STATUS 0x0A
#define CMD_SCAN        0x0C
#define CMD_PAIR        0x0E
#define CMD_UNPAIR      0x10
#define CMD_LIST_PAIRED 0x12
#define CMD_CHN_STATUS  0x14

/*
Channels
*/
#define CHANNEL_A  0x00
#define CHANNEL_B 0x01

/*
Channel state byte is a bitfield with
the following fields:
*/
enum Channel_State_Flags{
  CHANNEL_ID = 1 << 0,
  MUTED      = 1 << 1,  
  CONNECTED  = 1 << 2
};

/*
Ensure that the microcontroller supports Bluetooth
*/
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

#if !defined(CONFIG_BT_SPP_ENABLED)
#error Serial Bluetooth not available or not enabled. It is only available for the ESP32 chip.
#endif

/*
Class for the PGA2311 volume control chip.
*/
class PGA2311{
  private:
  /*
  Pins:
  */
  uint8_t cs; //Chip select
  uint8_t clk; //Clock
  uint8_t sd; //Serial data
  uint8_t mute_pin; //Mute

  public:
  PGA2311(){
    /*Dummy constructor so unassigned PGA2311 variables can be made.*/
  }

  PGA2311(uint8_t cs, uint8_t clk, uint8_t sd, uint8_t mute_pin){
    this->cs = cs;
    this->clk = clk;
    this->sd = sd;
    this->mute_pin = mute_pin;
  }

  void init(){
    /*
    Configure pins
    */
    pinMode(cs, OUTPUT);
    pinMode(clk, OUTPUT);
    pinMode(sd, OUTPUT);
    pinMode(mute_pin, OUTPUT);

    /*
    Set pins to default states
    */
    digitalWrite(cs, HIGH);
    digitalWrite(clk, HIGH);
    digitalWrite(sd, HIGH);
    digitalWrite(mute_pin, HIGH); //Mute pin is active low; default to unmited.
  }

  void send_vol(uint8_t l_vol, uint8_t r_vol){
    /*
    Sends set the gain of the volume control chip.

    The chip handles stereo audio, l_vol and r_vol correspond
    to the left and right channels, where a volume level is represented
    as one byte of data (0-255). 127 is unity gain.
    */

    /*
    Pull the chip select low to begin:
    */
    digitalWrite(cs, LOW);

    /*
    Take the left and right volume (gain) data
    and put them into a single int value for transmission
    */
    uint16_t vol = 0x0000;
    vol |= (r_vol << 8);
    vol |= l_vol;

    /*
    Gain values are sent as follows:

    8 bits for right channel immediately followed by
    8 bits for left channel.

    A clock transition from high to low signifies the
    end of a bit.
    */
    for(int i = 0; i < 16; i++){
      digitalWrite(clk, HIGH);
      digitalWrite(sd, (vol & 0x8000) >> 15);
      digitalWrite(clk, LOW);
      vol = vol << 1;
    }

    /*
    Reset pins once we're done.
    */
    digitalWrite(cs, HIGH);
    digitalWrite(clk, HIGH);
    digitalWrite(sd, HIGH);
    digitalWrite(mute_pin, HIGH);
  }

  void mute(){
    /*
    Mute is active low.
    */
    digitalWrite(mute_pin, LOW);    
  }

  void unmute(){
    digitalWrite(mute_pin, LOW);
  }
    
};

/*
Class for a stereo channel.

The BlueBox supports two stereo channels (speakers), each of which
uses one PGA2311 to modulate the gain.
*/
class Channel{
  private:
  uint8_t state_bits;     //Stores data like channel ID, mute, connection status, etc. See Channel_State_Flags for the meaning of each bit.
  uint8_t vol;            //Current channel volume
  uint8_t tx_connect_pin; //CON pin on the associated transmitter. This is used for putting the transmitter into pair mode.
  PGA2311 control_chip;   //The colume control chip for this stereo channel.
  
  public:
  Channel(uint8_t channel_id, uint8_t vc_cs, uint8_t vc_clk, uint8_t vc_sd, uint8_t vc_mute, uint8_t tx_connect){
    state_bits = channel_id;
    control_chip = PGA2311(vc_cs, vc_clk, vc_sd, vc_mute);
    tx_connect_pin = tx_connect;
  }

  void init(){
    pinMode(tx_connect_pin, OUTPUT);
    control_chip.init();
    set_vol(127); //Default to unity gain.
  }

  /* buf should be of size 2.*/
  void get_state(uint8_t* buf){
    /*
    Getter for the volume and other channel state data.

    buf should be a buffer of two bytes. The first byte will be the state bits,
    the second byte will be the volume.
    */
    buf[0] = state_bits;
    buf[1] = vol;
  }

  void set_vol(uint8_t new_vol){
    /*
    Set the volume of the associated speaker
    */
    control_chip.send_vol(vol, vol);
    vol = new_vol;  
  }

  void mute(){
    control_chip.mute();     
    state_bits |= MUTED; 
  }  

  void unmute(){
    control_chip.unmute();
    state_bits &= ~(MUTED);
  }

  void connect(){
    /*
    Hold the CON pin on the transmitter high for 3 seconds
    to make it enter pair mode.
    */
    digitalWrite(tx_connect_pin, HIGH);
    delay(3000);
    digitalWrite(tx_connect_pin, LOW);
  }
  
};

BluetoothSerial SerialBT;

Channel channel_a  = Channel(CHANNEL_A, CH1_VC_CS, CH1_VC_CLK, CH1_VC_SD, CH1_VC_MUTE, CH1_TX_CONNECT);
Channel channel_b =  Channel(CHANNEL_B, CH2_VC_CS, CH2_VC_CLK, CH2_VC_SD, CH2_VC_MUTE, CH2_TX_CONNECT);

void setup() {
  Serial.begin(115200);  
  SerialBT.begin("BlueBox"); //Bluetooth device name

  channel_a.init();
  channel_b.init();
}

uint8_t BT_read_byte(){
  /*
  Read one byte of Bluetooth data from the phone.
  */
  while(SerialBT.available() == 0){}
  return static_cast<uint8_t>(SerialBT.read());
}

void BT_read_bytes(uint8_t* out_bytes, uint32_t num_bytes){
  /*
  Read num_bytes number of bytes of BT data from the phone.
  */
  uint32_t num_bytes_read = 0;
  while(num_bytes_read < num_bytes){
    out_bytes[num_bytes_read++] = BT_read_byte();
  }
}

void loop() {
  /*
  Wait for a Bluetooth command:
  */
  uint8_t data = BT_read_byte();

  /*
  Separate the command ID and the channel ID
  from the sent byte:
  */
  uint8_t cmd  = data & 0xFE;
  uint8_t chn  = data & 0x01;

  switch(cmd){
    case CMD_SET_VOL:
    {
      /*
      Grab the volume byte sent from the phone and adjust the relevant channel:
      */
      uint8_t data = BT_read_byte();
      switch(chn){
        case CHANNEL_A:
          channel_a.set_vol(data);
          break;
        case CHANNEL_B:
          channel_b.set_vol(data);
          break;
      }
    }
      break;

    case CMD_MUTE:
  
      switch(chn){
        case CHANNEL_A:
          channel_a.mute();          
          break;
        case CHANNEL_B:
          channel_b.mute();
          break;
      }
      break;

    case CMD_UNMUTE:

      switch(chn){
        case CHANNEL_A:
          channel_a.unmute();          
          break;
        case CHANNEL_B:
          channel_b.unmute();
          break;
      }
      break;

    case CMD_CONNECT:
    {

      switch(chn){
        case CHANNEL_A:
          channel_a.connect();          
          break;
        case CHANNEL_B:
          channel_b.connect();
          break;
      }
      break;

    }

    case CMD_CHN_STATUS:
      {
        /*
        Send the channel statuses back to the phone:
        */
        uint8_t status_buf[5] = {data, 0x00, 0x00, 0x00, 0x00};
        channel_a.get_state(status_buf + 1);
        channel_b.get_state(status_buf + 3);
        SerialBT.write(status_buf, 5);          
      }
      break;

    default:
      Serial.println("Unknown command " + String(static_cast<int>(cmd)));
  }  
  
}
